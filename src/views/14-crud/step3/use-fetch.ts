import { useEffect, useState } from 'react';
import { fetchDelete, fetchGet, fetchPost } from './http-utils';

interface fetchType {
  id?: number;
  name: string;
}


// TODO type as generic
export const useFetch = <T extends fetchType >(initialURL: string): any => {
  const [data, setData] = useState<T[]>([]);
  const [error, setError] = useState<boolean>(false);

  useEffect(() => {
    getHandler(initialURL);
  }, [initialURL]);

  const getHandler = async (API_URL: string) => {
    setError(false);
    try {
      const response = await fetchGet(API_URL);
      setData(response)
    } catch (e) { setError(true) }
  };

  const postHandler = async (API_URL: string, item: T) => {
    setError(false);
    try {
      const response = await fetchPost(API_URL, item);
      setData(prevState => [...prevState, response]);
    } catch (e) { setError(true) }
  };

  const deleteHandler = async (API_URL: string, id: number) => {
    setError(false);
    try {
      await fetchDelete(API_URL, id);
      setData(prevState => prevState.filter(item => item.id !== id))
    } catch (e) { setError(true) }
  };

  return {
    data,
    error,
    http: {
      get: getHandler,
      post: postHandler,
      delete: deleteHandler,
    }
  }
};
