
const headers = {
  'Content-Type': 'application/json; charset=utf-8'
};

function dispatchServerError() {
  throw new Error('Server unreachable.')
}

// export const fetchPost = async <T extends { id: number}>(item: T) => {
export const fetchGet = async(url: string) => {
  let response;
  try {
    response = await fetch(url, { method: 'GET' },
    )
  } catch (e) { throw new Error('Server unreachable.') }

  if (!response.ok) {
    dispatchServerError()
  }

  return response.json()
};

export const fetchPost = async <T>(url: string, item: T) => {
  let response;
  try {
    response = await fetch(
      url, {
        method: 'POST',
        headers,
        body: JSON.stringify(item)
      },
    )
  } catch (e) { throw new Error('Server unreachable.') }

  if (!response.ok) {
    dispatchServerError()
  }

  return response.json()
};

export const fetchDelete = async (url: string, id: number) => {
  let response;
  try {
    response = await fetch(
      `${url}/${id}`, {
        method: 'DELETE',
        headers
      },
    )
  } catch (e) { throw new Error('Server unreachable.') }

  if (!response.ok) {
    dispatchServerError()
  }

  return response.json()
};
