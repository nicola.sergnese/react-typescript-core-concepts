import React, { useCallback } from 'react';
import { useWindowEvent } from './hooks/useWindowEvent';
import { useMouseUp } from './hooks/useMouseUp';
import { useWindowScroll } from './hooks/useWindowScroll';

export const Demo01CustomHooks: React.FC = () => {
  // const [widowWidth, setWindowWidth] = useState(window.innerWidth);

  // 1. simple hook
  const cb = ((e: any) => {
    console.log('resize', e)
  });
  useMouseUp(cb);

  // 2. reusable hook
  const callback = useCallback(e => {
    console.log('mouseup', e);
  }, []);
  useWindowEvent('mouseup', callback);

  // 3. hook that uses useWindowEvent inside
  const callbackScroll = useCallback(e => {
    console.log('scroll', e);
  }, []);
  useWindowScroll(callbackScroll);

  return <h1>
    Custom hooks
  </h1>
};
