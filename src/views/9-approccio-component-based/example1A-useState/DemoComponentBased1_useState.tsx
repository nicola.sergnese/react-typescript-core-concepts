import React, { useEffect, useState } from 'react';
import { Form } from '../shared/components/Form';
import { List } from '../shared/components/List';
import { Product } from '../shared/model/product';

const API_URL = 'http://localhost:3001/products';

// demo with useState

const initialActiveState: Product = ({ name: '', price: 0}) as Product;
const initialProductsState: Product[] = [];

export const DemoComponentBased1: React.FC = () => {
  const [active, setActive] = useState<Product>(initialActiveState);
  const [products, setProduct] = React.useState<Product[]>(initialProductsState);

  useEffect(() => {
    (async function () {
      const response = await fetch(API_URL, { method: 'GET' });
      if (response.ok) {
        setProduct(await response.json());
      }
    })()
  }, []);


  const deleteProductHandler = async (product: Product) => {
    const response = await fetch(`${API_URL}/${product.id}`, { method: 'DELETE' });
    if (response ) {
      await response.json()
    }
    if (response.ok) {
      setProduct(products.filter(u => u.id !== product.id));
      if (product.id === active.id) {
        setActive({...initialActiveState});
      }
    }
  };

  const saveProductHandler = (product: Product) => {
    if (active.id) {
      editProductHandler(product);
    } else {
      addProductHandler(product);
    }
  };

  const addProductHandler = async (product: Product) => {
    const response = await fetch(API_URL, {
      method: 'POST',
      headers: { 'Content-Type': 'application/json; charset=utf-8' },
      body: JSON.stringify(product)
    });

    if (response.ok) {
      const product = await response.json()
      setProduct([...products, product]);
      setActive({...initialActiveState});
    }
  };

  const editProductHandler = async (product: Product) => {
    const response = await fetch(`${API_URL}/${product.id}`, {
      method: 'PATCH',
      headers: { 'Content-Type': 'application/json; charset=utf-8' },
      body: JSON.stringify(product)
    });

    if (response.ok) {
      const newUser = await response.json();
      setProduct(
        products.map(u => {
          return u.id === product.id ? newUser : u;
        })
      );
    }
  };

  const setActiveHandler = (product: Product) => {
    setActive(product);
  };

  return (
    <>
      <Form
        active={active}
        onAdd={saveProductHandler}
        onReset={() => setActive(initialActiveState)}
      />
      <hr/>
      <List
        active={active}
        product={products}
        onDelete={deleteProductHandler}
        onSetActive={setActiveHandler}
      />
    </>
  )
};
