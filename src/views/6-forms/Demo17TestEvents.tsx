// Not linked in the menu
import React, { useRef, useState } from 'react'

export const DemoMouseEvents: React.FC = () => {
  const [value, setValue] = useState('ciccio');
  const inputEl = useRef<HTMLInputElement>(null);

  const onKeyPress = (e: React.KeyboardEvent<HTMLInputElement>) => {
    // e.preventDefault();
    console.log(e.key);
    console.log(e.currentTarget.type, e.currentTarget.value);
    setValue(e.currentTarget.value)
  };

  const onChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    // e.preventDefault();
    // console.log(e.key) // does not exist
    console.log((e.nativeEvent as InputEvent));
    console.log(e.currentTarget.type, e.currentTarget.value);
    setValue(e.currentTarget.value)
  };

  // const onChange = (e: React.FormEvent<HTMLInputElement>) => {
  const onChangeSynthetic = (e: React.SyntheticEvent<HTMLInputElement>) => {
    e.preventDefault();
    console.log(e.nativeEvent, e.currentTarget.type, e.currentTarget.value);
    setValue(e.currentTarget.value);
  };

  // console.log(value)
  return (
    <div className="example-box-centered">
      <input
        ref={inputEl}
        type="text"
        onKeyPress={onKeyPress}
      />

      <input
        type="text"
        value={value}
        onChange={onChange}
        placeholder="Panel title"
      />

      <input
        type="text"
        value={value}
        onChange={onChangeSynthetic}
        placeholder="Panel title"
      />

      <hr/>
    </div>
  )
};
