import React, { useState } from 'react';

export const DemoToggable: React.FC = () => {
  return (
    <>
      <Toggable title="Hello Toggable (click me)">
        <input type="text" className="form-control"/>
      </Toggable>
    </>
  )
};

/////////////////////////
// TOGGABLE COMPONENT
/////////////////////////

interface PanelProps {
  title?: string;
  children: React.ReactNode
  // style: --> All HTML elements are supported since below we extend HTMLProps
}

export const Toggable: React.FC<PanelProps & React.HTMLProps<HTMLDivElement>> = ({ title, children, style }) => {
  const [opened, setOpened] = useState<boolean>(true);

  return (
    <div className="card" style={{...style}}>
      <div
        className="card-header"
        style={{ cursor: 'pointer'}}
        onClick={() => setOpened(!opened)}
      >{title}</div>
      { opened && <div className="card-body">{children}</div> }
    </div>
  )
};

