import React from 'react';

export const DemoMapQuest: React.FC = () => {
  return (
    <>
      <MapQuest city="Trieste" width={300} height={200} />
      <hr/>
      <MapQuest city="Rome" />
      <hr/>
      <MapQuest city="Milan" width={600} height={40}/>
    </>
  )
};

/////////////////////////
// MAPQUEST COMPONENT
/////////////////////////

interface MapProps extends React.ImgHTMLAttributes<HTMLImageElement> {
  city: string;
  // NOTE:
  // width, height not necessary because we exten
}

const API = 'https://www.mapquestapi.com/staticmap/v5/map?key=Go3ZWai1i4nd2o7kBuAUs4y7pnpjXdZn';

// Support HTMLImage props -> solution 1: inline extends ImgHTMLAttributes:
// export const MapQuest: React.FC<MapProps & React.ImgHTMLAttributes<HTMLImageElement>> = ({

// Support HTMLImage props -> solution 2: interface extends  ImgHTMLAttributes:
export const MapQuest: React.FC<MapProps> = ({
  city,
  width = 400,
  height = 100,
  ...rest
}) => {
  const url = `${API}&size=${width},${height}&center=${city}`;
  return <img src={url} alt={city} {...rest}/>;
};

